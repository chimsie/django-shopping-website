This is our item application made by

sirine aoudj, david nguyen, estefan maheux-saban and jessie rambeloson

To use this app you can log in as nasr with the password 1234

Admin app (made by David):
	AdministrationApp sets up administration privileges and enables admins to use them.
	Administration privileges consist of adding, deleting, editing users as well as changing their group.
	Admins can also block users from accessing the website but will not delete their products or information.
	As for products, admins can edit any product or delete them.
	There are three groups of admins. Admin_gp have all administrative powers. 
	Admin_user_gp can only manage users who are members. 
	Admin_item_gp have admin_user_gp privileges and they can also manage items of members.

		The nasr user is part of the admin_gp group.

UserManagement app (made by Sirine):
	We have usermanagement (made by sirine) app which basically allows anyone to register (when you are registed you are automatically added to the members group)
	to be added to any other group you need to ask and admin.

	Any registered user can:
 	-login witht heir credentials
 	-look at their profile
 	-edit their profile
 	-reset their password 
 	-change their profile picture
 	-add a product
 	-send a message to another user
 	-see their message inbox
 	-comment, like, flag a product

MessageApp (made by Estefan):
	The MessageApp  preocuppies most of the messaging system between users.
	It gives a ui to the user to view all the messages received from different user or themselves.
	The messages received are in a form of list where the user can scroll the page to view each message.
	They are able to send a message with a title, content and recipient.
	The action is done through a button that redirects them to a page where they can 
	input the necessery fields to post the message to the desired user.

ProductApp (made by Jessie):
The ItemApp contains all the modules concerning the products.
	When logged in, the user can:
	- list/order/filter all the products
	- view the details about a product
	- add a new product
	- rate/flag/like a product
	- comment on a product

	When logged out, the user cannot:
		- rate/flag/like a product
	- add a new product
	- comment on a product

	The app has 2 models:
	- Product, which has 2 functions:
    	- total_likes that returns the product's amount of likes to display on the details
    	- total_flags that returns the product's amount of flags
	- Comment with has a function that returns the username of the person who commented.
	
	The app has multiple views:
	- ProductsList: displays the list of all products in products.html
	- ProductDetails: displays the details of a selected product in product_details.html
	- NewProducts: displays only the products that have a 'New' status in products_html
	- UsedProducts: displays only the products that have a 'Used' status in products_html
	- OrderPriceLowHigh: orders the products displayed from the lowest to highest price in products_html
	- OrderPriceHighLow: orders the products displayed from the highest to lowest price in products_html
	- OrderRateLowHigh: orders the products displayed from the lowest to highest rate in products_html
	- OrderRateHighLow: orders the products displayed from the highest to lowest rate in products_html
	- OrderTitleAsc: orders the products displayed alphabetically in products_html
	- OrderTitleDesc: orders the products displayed from z to a in products_html
	- searchProducts: a function that returns the products that contain the keyword entered by the user in its name in search_products.html
	- LikeView: function that adds a like to the product
	- FlagView: function that adds a flag to the product
	- AddProduct: function that will save a new product based on the input in the AddNewProduct form in add_product.html
	There are a total of 3 forms, SendCommentModelForm that creates a new comment, AddNewProduct to add a new product and EditProduct to edit a new product.

	The urls.py module contains links that lead to every view from views.py and a path to the ratings stars to display on the product's details.

	The templates:
	- add_product.html: the form that allows user to enter a new product's information
	- post_comment.html: the form that allows user to enter their comment
	- product_details.html: displays a product's details and a comments section
	- products.html: displays all the products
	- search_products.html: displays the products resulted from a search


We each assigned an app per person but of course we all worked collectively on each part as we helped each other!

We have two gitlab since the first one got corrupted with merging so i made it public so you can look without being a member: https://gitlab.com/SirineA/project_2_python.git
You were added as a maintainer in the second, final gitlab.

You can access our app with the following link: https://dw-42021-prj-6-aoudj-web-app.herokuapp.com/AdministrationApp
