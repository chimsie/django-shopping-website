#UserMnagementApp/urls.py
from django.conf.urls import url, include
from django.urls import path
from . import views

urlpatterns =[
    path('',views.loginPage, name='index'),
    path('home', views.profilePage, name='home'),
    path('profile', views.profilePage, name='profile'),
    path('register', views.register, name='register'),
    path('login', views.loginPage, name='login'),
    path('logout', views.logout_user, name='logout'),
    path('edit_profile', views.edit_profile, name='edit_profile'),
    path('edit_picture', views.edit_picture, name='edit_picture'),
    path('change_password', views.change_password, name='change_password'),
] 