from django.forms.models import ModelForm
from UserManagement.models import UserProfile
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

class UserRegistration(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']

class EditUser(UserChangeForm):
    
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']

class EditProfile(ModelForm):
    
    class Meta:
        model = UserProfile
        fields = ['image']


