from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

# Create your models here.
#this extends the user model to have an image field
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='profile_image/profile_pic.png',upload_to="profile_image", blank=True)

    def __str__(self):
        return self.user.username

#this creates a profile for an user
def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])

#this makes it that everytime a user is created it also creates a user profile so it can have an image filed for its profile
post_save.connect(create_profile, sender=User)

