from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.models import User, Group
from django.contrib.auth import (
    authenticate,
    login,
    logout,
    update_session_auth_hash
)
from .forms import (
    UserRegistration,
    EditUser,
    EditProfile
)
from django.contrib.auth.forms import PasswordChangeForm

# Create your views here.
#UserManagement was made and created by Sirine Aoudj, 1935903
#This is user management app that allows users to register,login,logout and edit their information.

def index(request):
    return render(request, 'UserManagement/index.html')

def home(request):
    return render(request, 'UserManagement/index.html')


#This function based view allows the user to register it takes the overriden form from the forms.py
def register(request):
    #it stores the user registration form
    form = UserRegistration()

    #then if the form has been submitted it stores the passwords
    if request.method == 'POST':
        form = UserRegistration(request.POST)
        password1 = request.POST.get("password1")
        password2 = request.POST.get("password2")

        #if the form is valid then it will save the user then add the user to members group
        #then redirect to login page
        if form.is_valid():
            user = form.save()
            group = Group.objects.get(name='members')
            user.groups.add(group)
            username = form.cleaned_data.get('username')
            messages.success(request, "Account was created for " + username)
            print("user created")
            return redirect('login')
        else:
            #if the passwords do not match it will display on the page that they do not match
            if password1 != password2:
                args = {"error_message":"Passwords do not match",'form':form}
                return render(request,'UserManagement/register.html',args)
            #if the password is too short, does not have numbers or is too easy it will display to the user
            else:
                args = {"error_message":"Password does not meet requirements",'form':form}
                return render(request,'UserManagement/register.html',args)

    context= {'form':form}
    return render(request,"UserManagement/register.html", context)

#this function allows the user to login and have functionality
def loginPage(request):
    #if the user fills the login form
    if request.method == 'POST':
        username = request.POST.get('username')
        password= request.POST.get('password')
        #we authenticate the user
        user = authenticate(request, username=username, password=password)

        #if the information is valid and the user exist it will bring the user to the home page(products)
        if user is not None:
            login(request, user)
            return redirect("home")
        #it will display to the user if it is not valid information
        else:
            messages.info(request, "Username or Password is incorrect...")
    context = {}
    return render(request, 'UserManagement/login.html',context)

#this function allows the user to logout, so it takes the user qith the request and uses the django logout
def logout_user(request):
    print('logout ran')
    logout(request)
    return redirect('products')

#This renders the profile page of the loged in user and passes the user to the profile page to display it's information
def profilePage(request):
    args = {'user': request.user}
    return render(request, 'UserManagement/profile.html', args)

#This allows to edit the profile, it uses the EditUser form
def edit_profile(request):
    if request.method == 'POST':
        #it passes the user to the form to fill the form when it is displayed
        form = EditUser(request.POST, instance=request.user)

        #when the form is submitted if the info is valid it will save it
        if form.is_valid():
            form.save()
            #messages.success(request, "Account was created for " + username)
            print("user modified")
            return redirect('profile')
    else:
        form = EditUser(instance=request.user)
        args = {'form':form}
        return render(request,'UserManagement/edit_profile.html', args)

#This allows to edit the user profile picture, it uses the editprofile form
def edit_picture(request):
    if request.method == 'POST':
        #it passes the userprofile because userrpofile stores an image field
        form = EditProfile(request.POST, request.FILES, instance=request.user.userprofile)

        #if the form is valid then it will save and reutnr the user to its profile
        if form.is_valid():
            form.save()
            #messages.success(request, "Account was created for " + username)
            print("user modified")
            return redirect('profile')
    else:
        form = EditProfile(instance=request.user.userprofile)
        args = {'form':form}
        return render(request,'UserManagement/edit_picture.html', args)

#This allows the user to chnage it's password
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        password1 = request.POST.get("new_password1")
        password2 = request.POST.get("new_password2")

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            #messages.success(request, "Account was created for " + username)
            print("user modified")
            return redirect('profile')
        #if the password are not valid it will display on the chnage password page
        else:
            if password1 != password2:
                args = {"error_message":"Passwords do not match",'form':form}
                return render(request,'UserManagement/change_password.html', args)
            else:
                args = {"error_message":"Password does not meet requirements",'form':form}
                return render(request,'UserManagement/change_password.html', args)
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form':form}
        return render(request,'UserManagement/change_password.html', args)


