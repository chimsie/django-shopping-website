from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm

class EditForm(UserChangeForm):
    password = None

    class Meta:
        model = User
        fields ={
            'username',
            'first_name',
            'last_name',
            'email'
        }
        help_texts={
            'username': None
        }

    field_order = ['username','first_name','last_name','email']


