from django.db import models
from django.contrib.auth.models import User
# Create your models here.

'''
class User(models.Model):
    userId = models.CharField(max_length=4)
    #placeholder for password
    password = models.CharField(max_length=26)
    username = models.CharField(max_length=26)
    userGroup = models.CharField(max_length=26)

    def __str__(self):
        return self.username
'''

class Log(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    type = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
