from django.apps import AppConfig


class AdministrationappConfig(AppConfig):
    name = 'AdministrationApp'
