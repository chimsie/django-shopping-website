from django.urls import path

from . import views
from UserManagement.views import loginPage
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', loginPage, name='login'),
    path('home', loginPage, name='login'),
    path('admin', views.admin_view.as_view(), name='admin'),
    path('logout', views.logout_user, name='logout'),
    #path('profile', views.profile_view.as_view(), name='profile'),
    path('admin/edit_profile', views.edit_profile, name='admin/edit_profile'),
    path('admin/edit_group', views.edit_group, name='admin/edit_group'),
    path('admin/block_user', views.block_user, name='admin/block_user'),
    path('admin/unblock_user', views.unblock_user, name='admin/unblock_user'),
    path('admin/delete_user', views.delete_user, name='admin/delete_user'),
    path('admin/add_user', views.add_user, name='admin/add_user'),
    path('admin/search_user', views.search_user, name='admin/search_user'),
    path('admin/admin_log', views.admin_log.as_view(), name='admin/admin_log'),
    path('admin/search_log', views.search_log, name='admin/search_log'),
    path('admin/user_products', views.user_products.as_view(), name='admin/user_products'),
    path('admin/edit_products', views.edit_product, name='admin/edit_products'),
    path('admin/delete_product', views.delete_product, name='admin/delete_product')
    #path('login', auth_views.LoginView.as_view(), name='login'),
    #path('logout', auth_views.LogoutView.as_view(), name='logout')
]