from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.models import User, Group
from AdministrationApp.models import Log
from .forms import EditForm
from django.views.generic.base import TemplateView
from django.contrib.auth import authenticate, login, logout
from django.core.paginator import Paginator
from ItemApp.models import Product
from ItemApp.forms import EditProductForm
import datetime

#AdministrationApp was made and created by David Nguyen, 1938087
#AdministrationApp is an appplication made to handle administrative powers

#loginTest is a function based view that would display a login form for testing administrative permissions.
def loginTest(request):
    if request.method == "POST":

        #retrieve username and password from form
        username = request.POST.get('username')
        password = request.POST.get('password')

        #authenticate with username and password
        user = authenticate(request, username=username, password=password)

        if user is not None:
            #if login success, redirect to profile
            print("login success")
            login(request, user)
            return redirect('profile')
        else:
            print("login failed")


    return render(request, 'AdministrationApp/index.html')
    #return HttpResponse("Hello, world. You're at the AdministrationApp index.")

#logout_user is a function based view that logs a user out
def logout_user(request):
    print('logout ran')
    logout(request)
    return render(request, 'AdministrationApp/logout.html')

#edit_profile is a function based view that manages the edit profile view
def edit_profile(request):

    #retrieve user id through request.GET and fetch the user with the user id
    #stores old information for logging purposes
    user_id = request.GET.get('id')
    user_instance = User.objects.get(pk=user_id)
    old_user = user_instance.username
    old_first_name = user_instance.first_name
    old_last_name = user_instance.last_name
    old_email = user_instance.email

    if request.method == 'POST':

        #retrieves request.POST and sends it to editform as well as a user_instance in order to fill up the form
        form = EditForm(request.POST, instance=user_instance)
        if form.is_valid():
            #if form is valid, user changes will be saved
            form.save()
            #ADD TO LOG
            description = "(USERID: "+user_id+") "

            if(old_user != request.POST.get('username')):
                description += "username: "+old_user+" --> "+request.POST.get('username')+"; "
            if(old_first_name != request.POST.get('first_name')):
                description += "first_name: "+old_first_name+" --> "+request.POST.get('first_name')+"; "
            if(old_last_name != request.POST.get('last_name')):
                description += "last_name: " + old_last_name + " --> " + request.POST.get('last_name') + "; "
            if (old_email != request.POST.get('email')):
                description += "email: " + old_email + " --> " + request.POST.get('email') + "; "

            current_log = Log(user=request.user, date=datetime.date.today(), type="Edit Profile", description=description)
            current_log.save()
            #END LOG

            return redirect('admin')
        else:
            #if user already exist, redirect to same page with error message
            form = EditForm(instance=user_instance)
            args = {'form': form, 'title': "Edit", 'user': user_instance, 'error_message': "User already exist."}
            return render(request, 'AdministrationApp/edit_user.html', args)
    else:
        #if request is not POST, display user information in form
        form = EditForm(instance=user_instance)
        args = {'form': form, 'title': "Edit", 'user': user_instance}
        return render(request, 'AdministrationApp/edit_user.html', args)

#edit_group is a function based view  that manages the edit group feature
def edit_group(request):
    title = "Edit group"

    #retrieve user id and create user object
    user_id = request.GET.get('id')
    user = User.objects.get(pk=user_id)
    #get current group
    current_group_name = request.GET.get('group')

    if request.method == 'POST':

        #if group chosen was not none, then assign new group to selected user
        if request.POST.get('groups') is not None:
            #ADD TO LOG
            description = "(USERID: "+user_id+") "

            if current_group_name != request.POST.get('groups'):
                description+="group: "+current_group_name+" --> "+request.POST.get('groups')+"; "

            current_log = Log(user=request.user, date=datetime.date.today(), type="Edit Group", description=description)
            current_log.save()
            #END LOG

            #removes the user from the current group he/she is in
            current_group = Group.objects.get(name=current_group_name)
            current_group.user_set.remove(user)

            #adds the user to new group
            new_group_name = request.POST.get("groups")
            new_group = Group.objects.get(name=new_group_name)
            new_group.user_set.add(user)
        else:

            #if no option selected, returns an error message
            args = {'title': title, 'user': user, 'error_message': "Option not selected."}
            return render(request, "AdministrationApp/edit_group.html", args)

        #redirects to admin page once operation has been completed
        return redirect('admin')


    else:
        #if request method is not post, display the group selection
        args = {'title': title, 'user': user}
        return render(request, "AdministrationApp/edit_group.html", args)


#block_user is a function based view that lets admin block an user from accessing the website
def block_user(request):
    title = "Block user"
    #retrieve user id and fetch user object from that id
    user_id = request.GET.get('id')
    user = User.objects.get(pk=user_id)
    current_group_name = request.GET.get('group')

    #if request is post, get yes or no result
    if request.method == 'POST':
        #if no option was selected redirect user to same page with error message
        if request.POST.get('yesNo') == None:
            args = {'title': title, 'user': user, 'error_message': "Option not selected."}
            return render(request, 'AdministrationApp/block_user.html', args)
        else:
            #if answer is yes, block the selected user, else redirect back to admin page
            answer = request.POST.get('yesNo')
            if(answer == "no"):
                return redirect('admin')
            else:

                #ADD TO LOG
                description = "(USERID: " + user_id + ") BLOCKED."

                current_log = Log(user=request.user, date=datetime.date.today(), type="Block", description=description)
                current_log.save()
                #END LOG

                #remove user from current group
                current_group = Group.objects.get(name=current_group_name)
                current_group.user_set.remove(user)

                #add user to 'blocked' group
                block_group = Group.objects.get(name="blocked")
                block_group.user_set.add(user)

                return redirect('admin')


    else:
        #if request method is not post, display the block_user form
        args = {'title': title, 'user': user}
        return render(request, 'AdministrationApp/block_user.html', args)

#unblock_user is a function based view that lets admin unblock a user
def unblock_user(request):
    title = "Unblock user"
    # retrieve user id and fetch user object from that id
    user_id = request.GET.get('id')
    user = User.objects.get(pk=user_id)
    current_group_name = request.GET.get('group')

    # if request is post, get yes or no result
    if request.method == 'POST':
        # if no option was selected redirect user to same page with error message
        if request.POST.get('yesNo') == None:
            args = {'title': title, 'user': user, 'error_message': "Option not selected."}
            return render(request, 'AdministrationApp/unblock_user.html', args)
        else:
            # if answer is yes, unblock the selected user, else redirect back to admin page
            answer = request.POST.get('yesNo')
            if(answer == "no"):
                return redirect('admin')
            else:

                #ADD TO LOG
                description = "(USERID: " + user_id + ") UNBLOCKED."
                current_log = Log(user=request.user, date=datetime.date.today(), type="Unblock", description=description)
                current_log.save()
                #END LOG

                # remove user from blocked group
                current_group = Group.objects.get(name=current_group_name)
                current_group.user_set.remove(user)

                # add user to 'members' group
                members_group = Group.objects.get(name="members")
                members_group.user_set.add(user)

                return redirect('admin')


    else:
        args = {'title': title, 'user': user}
        return render(request, 'AdministrationApp/unblock_user.html', args)

#delete_user is a function based view that manages the delete user feature
def delete_user(request):
    title = "Delete user"
    # retrieve user id and fetch user object from that id
    user_id = request.GET.get('id')
    user = User.objects.get(pk=user_id)

    if request.method == 'POST':
        #if no option selected, display error message
        if request.POST.get('yesNo') == None:
            args = {'title': title, 'user': user, 'error_message': "Option not selected."}
            return render(request, 'AdministrationApp/delete_user.html', args)
        else:

            answer = request.POST.get('yesNo')
            #if answer is no, redirect to admin, else delete user
            if(answer == "no"):
                return redirect('admin')
            else:

                #ADD TO LOG
                description = "(USERID: " + user_id + ") DELETED."
                current_log = Log(user=request.user, date=datetime.date.today(), type="Delete", description=description)
                current_log.save()
                #END LOG

                user.delete()
                return redirect('admin')
    else:

        args = {'title': title, 'user': user}
        return render(request, 'AdministrationApp/delete_user.html', args)

#add_user is a function based view that manages the add user feature
def add_user(request):
    title = "Add user"

    if request.method == "POST":
        try:

            #retrieves user information
            username = request.POST.get('username')
            password = request.POST.get('password')
            first_name = request.POST.get('first_name')
            last_name = request.POST.get('last_name')
            email = request.POST.get('email_address')
            group_name = request.POST.get('groups')

            #user is created with the user information
            user = User(username=username, first_name=first_name, last_name=last_name, email=email)
            user.set_password(password)
            user.save()

            #adds user to member
            group = Group.objects.get(name=group_name)
            group.user_set.add(user)

            #ADD TO LOG
            description = "(NEW USER) "+username+" has been created and added into '"+group_name+"';"
            current_log = Log(user=request.user, date=datetime.date.today(), type="Add", description=description)
            current_log.save()
            #END LOG

            return redirect('admin')
        except:
            args = {'error_message':'User already exist.'}
            return render(request, 'AdministrationApp/add_user.html', args)

    return render(request, 'AdministrationApp/add_user.html')

#search_user is a function based view that returns the admin page with the searched user
def search_user(request):
    title = "Search user"

    if request.method == "POST":

        #retrieve username and displays the user in admin page
        username = request.POST.get("username")
        users = User.objects.filter(username=username)
        args = {'users': users}
        return render(request, 'AdministrationApp/admin.html', args)

    return render(request, 'AdministrationApp/search_user.html')

#search_log is a function based view that searches a log based off a username
def search_log(request):
    title = "Search user"

    if request.method == "POST":

        # retrieve username and displays the log from that user
        username = request.POST.get("username")
        logs = Log.objects.filter(user__username=username)
        args = {'logs': logs}
        return render(request, 'AdministrationApp/logs.html', args)

    return render(request, 'AdministrationApp/search_log.html')

#profile_view is a class based view that displays a user profile
class profile_view(TemplateView):
    template_name = "AdministrationApp/profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Profile"
        return context

#admin_view is a class based view that displays the admin page with all administrative features
class admin_view(TemplateView):
    template_name = "AdministrationApp/admin.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Admin"

        #retrieve current page number
        page_num = self.request.GET.get('page', 1)

        #retrieve the group name
        group_admin = Group(name="admin_gp")

        #if group name is 'admin_gp' then paginate all the users accordingly
        if self.request.user.groups.filter(name=group_admin):
            p = Paginator(User.objects.all(), 7)
            context['users'] = p.page(page_num)
        else:
            #else only paginate the members which 'admin_usr_gp' and 'admin_item_gp' only has access to.
            members = Group.objects.get(name='members').user_set.all()
            members |= Group.objects.get(name='blocked').user_set.all()
            p2 = Paginator(members, 7)
            context['members'] = p2.page(page_num)

        return context

#admin_log displays the log of all administrative operations with its user and description
class admin_log(TemplateView):
    template_name = "AdministrationApp/logs.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Admin logs"

        #paginate the log
        p = Paginator(Log.objects.all(), 10)
        page_num = self.request.GET.get('page', p.num_pages)
        context['logs'] = p.page(page_num)

        return context

#user_products is a class based view that displays all the products of a user
class user_products(TemplateView):
    template_name = "AdministrationApp/user_products.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_id = self.request.GET.get('id')
        user = User.objects.get(pk=user_id)
        context['object_list'] = Product.objects.filter(owner=user)
        context['user']=user
        return context

#edit_product is a function based view that allows admin to edit products
def edit_product(request):

    #retrieve product_instance from id
    product_id = request.GET.get('id')
    product_instance = Product.objects.get(pk=product_id)

    if request.method == 'POST':
        form = EditProductForm(request.POST, instance=product_instance)
        if form.is_valid():
            form.save()
            #ADD TO LOG
            description = "(PRODUCTID: "+product_id+") MODIFIED. "
            current_log = Log(user=request.user, date=datetime.date.today(), type="Edit Product", description=description)
            current_log.save()
            #END LOG

            return redirect('admin')
        else:
            return redirect('admin')
    else:
        form = EditProductForm(instance=product_instance)
        args = {'form': form, 'title': "Edit", 'product': product_instance}
        return render(request, 'AdministrationApp/edit_products.html', args)

#delete_product is a function based view that allows adm in to delete products
def delete_product(request):
    title = "Delete product"
    product_id = request.GET.get('id')
    product = Product.objects.get(pk=product_id)
    if request.method == 'POST':
        if request.POST.get('yesNo') == None:
            args = {'title': title, 'product': product, 'error_message': "Option not selected."}
            return render(request, 'AdministrationApp/delete_product.html', args)
        else:
            answer = request.POST.get('yesNo')
            if (answer == "no"):
                return redirect('admin')
            else:

                # ADD TO LOG
                description = "(PRODUCTID: " + product_id + ") DELETED."
                current_log = Log(user=request.user, date=datetime.date.today(), type="Delete",
                                  description=description)
                current_log.save()
                # END LOG


                product.delete()
                return redirect('admin')
    else:
        args = {'title': title, 'product': product}
        return render(request, 'AdministrationApp/delete_product.html', args)

def home(request):
    return render(request, 'AdministrationApp/index.html')
