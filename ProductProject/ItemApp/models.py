from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

'''
class Owner(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def owner_username(self):
        return self.user.get_username()
'''

# the product model
class Product(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True)	# owner of product
    name = models.CharField(max_length=200)		# name of the product
    genre = models.CharField(max_length=200)	# genre of product
    description = models.TextField()			# description of product
    price = models.DecimalField(max_length=6, max_digits=6, decimal_places=2)	# price of the product
    address = models.CharField(max_length=200)		# address of the owner/product
    status = models.CharField(max_length=200)		# status of product (new/used)
    rate = models.IntegerField()					# product's rate
    image = models.ImageField(default="product_image/question_mark.png", upload_to="product_image", blank=True ) # product's picture
    likes = models.ManyToManyField(User, related_name='product_likes')		# likes
    flags = models.ManyToManyField(User, related_name='product_flags')		# flags 

	# will return the amount of likes
    def total_likes(self):
        return self.likes.count()
	
	# will return the amount of flags
    def total_flags(self):
        return self.flags.count()

# the comment model
class Comment(models.Model):
    product = models.ForeignKey(Product, related_name='comments', on_delete=models.CASCADE)		# product commented
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='from_user_comment')	# user who commented
    body = models.TextField()	# the comment
    date_added = models.DateTimeField(auto_now_add=True)	# the date the comment was posted
	
	# will return the username of the person who commented
    def __str__(self):
        return '%s - %s' % (self.product.name, self.user.get_username())

'''
def create_owner(sender, **kwargs):
    if kwargs['created']:
        user_profile = Owner.objects.create(user=kwargs['instance'])

post_save.connect(create_owner, sender=User)
'''