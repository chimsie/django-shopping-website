from django.urls import path
from django.urls import include, re_path
from . import views

urlpatterns =[
    path('products', views.ProductsList.as_view(), name='products'),	# leads to a list of all products
    path('product_details/<int:pk>', views.ProductDetails.as_view(), name='product_details'),	# leads to the details of a selected product
    path('products-new', views.NewProducts.as_view(), name='new_products'),				# lists only the new products
    path('products-used', views.UsedProducts.as_view(), name='used_products'),			# lists only the products that have already been used
    path('search', views.searchProducts, name='search'),								# list the products that have the searched word in its name
    path('pricelowhigh', views.OrderPriceLowHigh.as_view(), name='price-low-high'),		# orders the list of products from lowest to highest price
    path('pricehighlow', views.OrderPriceHighLow.as_view(), name='price-high-low'),		# orders the list of products from highest to lowest price
    path('rate-low-high', views.OrderRateLowHigh.as_view(), name='rate-low-high'),		# orders the list of products from lowest to highest rate
    path('rate-high-low', views.OrderRateHighLow.as_view(), name='rate-high-low'),		# orders the list of products from highest to lowest rate
    path('title-asc', views.OrderTitleAsc.as_view(), name='title-asc'),					# orders the products alphabetically
    path('title-desc', views.OrderTitleDesc.as_view(), name='title-desc'),				# orders the products alphabetically reversed
    path('like/<int:pk>', views.LikeView, name='like_product'),							# links a like to its product
    path('flag/<int:pk>', views.FlagView, name='flag_product'),							# links a flag to its product
    path('add-product', views.AddProduct, name='add-product'),							# leads user to add a product
    path(r'^ratings/', include('star_ratings.urls', namespace='ratings')),
]
