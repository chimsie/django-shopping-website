from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.views.generic import ListView, DetailView
from .models import Product
from django.urls import reverse
from django.views.generic.edit import FormMixin
from .forms import SendCommentModelForm, AddNewProduct

# leads to the home page
def index(request):
    return render(request, 'ItemApp/index.html')

# leads to the home page
def home(request):
    return render(request, 'ItemApp/index.html')

# lists all products, showing 8 products/page
class ProductsList(ListView):
    model = Product
    paginate_by = 8
    context_object_name = 'products'
    template_name = 'ItemApp/products.html'
    queryset = Product.objects.all()

# display a single product's details
class ProductDetails(DetailView, FormMixin):
    model = Product
    context_object_name = 'product_details'
    template_name = 'ItemApp/product_details.html'
    form_class = SendCommentModelForm
	
    def get_success_url(self):
        return reverse('product_details', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super(ProductDetails, self).get_context_data(**kwargs)
        connected_actions = get_object_or_404(Product, id=self.kwargs['pk'])

        context['form'] = SendCommentModelForm(initial={'product': self.object})	# context for adding comments
        context['number_of_likes'] = connected_actions.total_likes()				# context to get the number of likes of the product
        context['number_of_flags'] = connected_actions.total_flags()				# context to get the number of flags of the product
        return context
	
	# adds the comment
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
	
	# saves the comment to the db
    def form_valid(self, form):
        print(form.cleaned_data)
        form.instance.product = get_object_or_404(Product, id=self.request.POST.get('product_id'))
        form.instance.user = self.request.user
        form.save()
        return super(ProductDetails, self).form_valid(form)

# filters the results with only new products
class NewProducts(ListView):
    model = Product
    paginate_by = 8
    context_object_name = 'products'
    template_name = 'ItemApp/products.html'
    queryset = Product.objects.all()
    def get_queryset(self):
       return Product.objects.all().filter(status__icontains='New')

# filters the results with only used products
class UsedProducts(ListView):
    model = Product
    paginate_by = 8
    context_object_name = 'products'
    template_name = 'ItemApp/products.html'
    queryset = Product.objects.all()
    def get_queryset(self):
       return Product.objects.filter(status__icontains='Used')

# orders the products by price from low to high
class OrderPriceLowHigh(ListView):
    model = Product
    paginate_by = 8
    context_object_name = 'products'
    template_name = 'ItemApp/products.html'
    queryset = Product.objects.all()
    ordering = ['price']

# orders the products by price from high to low
class OrderPriceHighLow(ListView):
    model = Product
    paginate_by = 8
    context_object_name = 'products'
    template_name = 'ItemApp/products.html'
    queryset = Product.objects.all()
    ordering = ['-price']

# orders the products from lowest rates to highest
class OrderRateLowHigh(ListView):
    model = Product
    paginate_by = 8
    context_object_name = 'products'
    template_name = 'ItemApp/products.html'
    queryset = Product.objects.all()
    ordering = ['rate']

# orders the products from highest rates to lowest
class OrderRateHighLow(ListView):
    model = Product
    paginate_by = 8
    context_object_name = 'products'
    template_name = 'ItemApp/products.html'
    queryset = Product.objects.all()
    ordering = ['-rate']

# orders the products by ascending title
class OrderTitleAsc(ListView):
    model = Product
    paginate_by = 8
    context_object_name = 'products'
    template_name = 'ItemApp/products.html'
    queryset = Product.objects.all()
    ordering = ['name']

# orders the products by ascending title
class OrderTitleDesc(ListView):
    model = Product
    paginate_by = 8
    context_object_name = 'products'
    template_name = 'ItemApp/products.html'
    queryset = Product.objects.all()
    ordering = ['-name']

# search a product
def searchProducts(request):
    if request.method == 'POST':
        search_product = request.POST['search_product']
        products = Product.objects.filter(name__icontains=search_product)
        return render(request, 'ItemApp/search_products.html', {'search_product': search_product, 'products': products})
    else:
        return render(request, 'ItemApp/search_products.html', {})

# adds like to the db
def LikeView(request, pk):
    product = get_object_or_404(Product, id=request.POST.get('product_like'))
    product.likes.add(request.user)
    return HttpResponseRedirect(reverse('product_details', args=[str(pk)]))

# adds flag to the db
def FlagView(request, pk):
    product = get_object_or_404(Product, id=request.POST.get('product_flag'))
    product.flags.add(request.user)
    return HttpResponseRedirect(reverse('product_details', args=[str(pk)]))

# adds and saves the new product
def AddProduct(request):
    if request.method == 'POST':
        form = AddNewProduct(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            new_product = Product.objects.get(description=request.POST.get('description'))
            new_product.owner = request.user
            new_product.save()
            return redirect('products')
        else:
            print(form.errors)

    form = AddNewProduct()
    context = {'form': form}
    return render(request, "ItemApp/add_product.html", context)
