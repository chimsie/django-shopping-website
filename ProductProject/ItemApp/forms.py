from django import forms
from django.forms import ModelForm
from django.http import HttpRequest
from .models import Comment, Product
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin

# creates the form in charge of comments
class SendCommentModelForm(LoginRequiredMixin, ModelForm):
    class Meta:
        model =Comment
        fields = ['body']

# creates the form in charge of adding products
class AddNewProduct(LoginRequiredMixin, ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'genre', 'description', 'price', 'address','rate', 'status', 'image']

# creates the form in charge of editing a product
class EditProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'genre', 'description', 'price', 'address', 'rate', 'status', 'image']
