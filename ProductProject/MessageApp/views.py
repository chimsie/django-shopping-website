from django.db.models import fields
from django.shortcuts import render, redirect
from django.urls.base import reverse_lazy
from django.views.generic import TemplateView, CreateView, ListView
from django.http import HttpRequest
from .models import theMessage
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from .forms import SendModelForm
from django.contrib import messages


# Create your views here.
#test template for basic.html
class MessagePageView(TemplateView):
    template_name = "MessageApp/message_ui.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = 'placeholder'
        return context

#list the messages of the user by date order
class MessageListView(ListView):
    model = theMessage
    ordering = ['date']

    def get_queryset(self):
        user = self.request.user
        return theMessage.objects.filter(to_id=user).order_by('-date')

#Ables the user to send a new message to a member
class MessageCreateView(LoginRequiredMixin, CreateView):
    template_name = 'MessageApp/themessage_create.html'
    form_class = SendModelForm
    queryset = theMessage.objects.get_queryset()
    success_url = reverse_lazy('view-msg')

    def get_queryset(self):
        user = self.request.user
        return theMessage.objects.filter(to_id=user)

    def form_valid(self, form):
        print(form.cleaned_data)
        form.instance.source_id = self.request.user
        messages.add_message(self.request, messages.INFO, form.instance.content)
        return super().form_valid(form)



