from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User, Group
from django.urls import reverse


# Create your models here.

#model to store and send messages between users
class theMessage(models.Model):
    source_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='from_user')
    content = models.TextField(max_length=1000, help_text="content")
    to_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='to_user')
    date = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=150)


