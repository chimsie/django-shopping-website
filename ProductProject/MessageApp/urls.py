from django.contrib.auth.views import LoginView
from django.urls import path
from django.urls.resolvers import URLPattern

from .views import *
from . import views

#the url to visit the views - inbox makes the user view his messages - send permits the user to send messages
urlpatterns =  [
    path('messages/inbox/', MessageListView.as_view(), name='view-msg' ),
    path('messages/send/', MessageCreateView.as_view() , name='send-msg')
]