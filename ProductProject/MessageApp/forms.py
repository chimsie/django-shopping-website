from django.views.generic import CreateView
from django import forms
from django.forms import ModelForm
from django.http import HttpRequest
from .models import theMessage
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin

#Creates the form that let the user input the required information to send a message
class SendModelForm(LoginRequiredMixin, ModelForm):

    class Meta:
        model = theMessage
        fields =['title','content','to_id']


