appdirs==1.4.4
asgiref==3.3.1
distlib==0.3.1
Django==3.1.7
django-heroku==0.3.1
django-star-ratings== 0.9.1
django-env==0.2.1
filelock==3.0.12
gunicorn==20.1.0
numpy==1.20.1
pytz==2021.1
six==1.15.0
sqlparse==0.4.1
virtualenv==20.4.4
virtualenvwrapper-win==1.2.6
psycopg2==2.8.6
Pillow==2.2.1